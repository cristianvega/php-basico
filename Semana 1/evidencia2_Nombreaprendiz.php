<!--
  Nombre aprendiz
  Desarrollo  Web con PHP
  Taller uso de arreglos
 -->
<!DOCTYPE html>
<html>
	<head>
		<title>Evidencia uso de arreglos</title>
		<meta http-equiv="Content-Type"
          	  content="text/html; charset=ISO-8859-1" />
	</head>
	<body>
		<h2><center>Evidencia uso de arreglos</h2></center>
		<br>
    <!--Creación de la tabla-->
		<table border="1" align="center" > 
    	 <!-- Etiquetas de tabla-->
      	<tr>
        <th>Nombre</th>
        <th>Dirección</th>
        <th>Teléfono</th>
        <th>Fecha de Cumpleaños</th>
        <th>Color Favorito</th>
        <th>Significado</th>
      	</tr>
    <?php
      /*Se construirá los arreglos multidimensional con los datos de las 3 personas contiene los siguientes datos: 
        Nombre, Dirección, Teléfono, Fecha de Cumpleaños, Color Favorito*/
        $listado = array( 
                array('Nombre' => "Juan Pérez", 'Dirección' => "Cra. 45 # 45-56", 'Teléfono' => "3456789", 'Fecha de cumpleaños' => "23/12/1997", 'Color favorito' => "Amarillo" ),
                array('Nombre' => "Pablo Manrique", 'Dirección' => "Clle. 23 # 12-19 sur", 'Teléfono' => "3214567", 'Fecha de cumpleaños' => "12/10/1980", 'Color favorito' => "Verde" ),
                array('Nombre' => "Nancy Peña", 'Dirección' => "Av. 34 # 16-12",  'Teléfono' => "2135423", 'Fecha de cumpleaños' => "07/06/2000" , 'Color favorito' => "Rojo" ),
                );
      //Arreglo para el significados de los colores
        $colores = array( 
                    array('Color favorito' => "Amarillo", 'Significado' => "Riqueza y alegría."),
                    );
      ?>
    <?php  
      //Ciclo foreach, para la impresión de los datos en la tabla
      foreach ($listado as $personas => $dato) {
        echo "<tr></tr>";
          foreach ($dato as $contenido) {
          echo ("<td> $contenido </td>");
          }
      }      
    ?>
  </body>
</html>